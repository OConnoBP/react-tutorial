import React from "react";
import { StyleSheet, css } from "aphrodite";

export default function Square (props) {
    return (
        <button className={css(styles.square, props.winner ? styles.winner : '')} onClick={props.onClick}>
            {props.value}
        </button>
    );
}

const styles = StyleSheet.create({
    winner: {
        background: 'yellow'
    },
    square: {
        ':focus': {
            outline: 'none'
        },
        background: '#fff',
        border: '1px solid #999',
        float: 'left',
        fontSize: '24px',
        fontWeight: 'bold',
        lineHeight: '34px',
        height: '34px',
        marginRight: '-1px',
        marginTop: '-1px',
        padding: '0',
        textAlign: 'center',
        width: '34px'
    }
});
