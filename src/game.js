import React from "react";
import Board from "./board.js";
import { StyleSheet, css } from "aphrodite";

const WINNER_SQUARES = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
];

export default class Game extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            history: [{
                squares: Array(9).fill(null),
                step: 0
            }],
            currentStepNumber: 0,
            xIsNext: true,
            sortOrder: 'asc'
        };
    }

    handleClick (i) {
        const history = this.state.history.slice(0, this.state.currentStepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();
        if (calculateWinner(squares) || squares[i]) {
            return;
        }

        squares[i] = this.state.xIsNext ? 'X' : 'O';

        const col = (i % 3) + 1;
        const row = Math.floor(i / 3) + 1;

        this.setState({
            history: history.concat([{
                squares: squares,
                col: col,
                row: row,
                step: history.length
            }]),
            currentStepNumber: history.length,
            xIsNext: !this.state.xIsNext
        });
    }

    jumpTo (step) {
        this.setState({
            currentStepNumber: step,
            xIsNext: (step % 2) === 0
        });
    }

    toggle () {
        const order = this.state.sortOrder === 'asc' ? 'desc' : 'asc';
        this.setState({
            sortOrder: order
        });
    }

    render () {
        const history = this.state.history;
        const current = history[this.state.currentStepNumber];
        const winner = calculateWinner(current.squares);
        const winnerSquares = getWinningSquares(current.squares);

        const moves = history.slice().sort((a, b) => {
            return this.state.sortOrder === 'asc' ? a.step - b.step : b.step - a.step;
        }).map((step, move) => {
            const desc = step.step ? `Go to move #${step.step} - (${step.col}, ${step.row})` : `Go to game start`;
            return (
                <li key={move}>
                    <button
                        onClick={() => this.jumpTo(step.step)}
                        style={{fontWeight: this.state.stepNumber === step.step ? 'bold' : ''}}>
                        {desc}
                    </button>
                </li>
            );
        });

        let status;

        if (winner) {
            status = `Winner: ${winner}`;
        } else {
            if (this.state.currentStepNumber >= 9) {
                status = `Draw!`;
            } else {
                status = `Current player: ${this.state.xIsNext ? 'X' : 'O'}`;
            }
        }

        return (
            <div className={css(styles.game)}>
                <div className="game-board">
                    <Board
                        squares={current.squares}
                        onClick={i => this.handleClick(i)}
                        winnerSquares={winnerSquares}/>
                </div>
                <div className={css(styles.gameInfo)}>
                    <div className={css(styles.status)}>{status}</div>
                    <button
                        onClick={() => this.toggle()}>
                        Sort: {this.state.sortOrder}
                    </button>
                    <ol>{moves}</ol>
                </div>
            </div>
        );
    }
}

function getWinningSquares (squares) {
    for (let i = 0; i < WINNER_SQUARES.length; i++) {
        const [a, b, c] = WINNER_SQUARES[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return WINNER_SQUARES[i];
        }
    }
    return null;
}

function calculateWinner (squares) {
    for (let i = 0; i < WINNER_SQUARES.length; i++) {
        const [a, b, c] = WINNER_SQUARES[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return squares[a];
        }
    }
    return null;
}

const styles = StyleSheet.create({
    game: {
        display: 'flex',
        flexDirection: 'row'
    },
    gameInfo: {
        marginLeft: '20px'
    },
    status: {
        marginBottom: '10px'
    }
});

