import React from "react";
import Square from "./square.js";
import { StyleSheet, css } from "aphrodite";

export default class Board extends React.Component {
    renderSquare (i, winner) {
        return (
            <Square
                key={i}
                value={this.props.squares[i]}
                winner={winner}
                onClick={() => this.props.onClick(i)}
            />
        );
    }

    render () {
        let board = [];
        let i = 0;

        while (i < 9) {
            let col = 0;
            let row = [];

            while (col < 3) {
                if(this.props.winnerSquares && this.props.winnerSquares.includes(i)) {
                    row.push(this.renderSquare(i, true));
                } else {
                    row.push(this.renderSquare(i, false));
                }

                i++;
                col++;
            }

            board.push(<div key={board.length} className={css(styles.boardRow)}>{row}</div>);
        }

        return (
            <div>
                {board}
            </div>
        );
    }
}

const styles = StyleSheet.create({
    boardRow: {
        ':after': {
            clear: 'both',
            content: '',
            display: 'table'
        }
    }
});
